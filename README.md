# 🤖 Auditor de Segurança Docker

Realizando auditorias de segurança automatizadas no Docker 👨‍💻

## O que ele faz 🧐

O Auditor de Segurança Docker inspeciona imagens Docker atrás de vulnerabilidades e configurações inseguras. Ele realiza testes automatizados incluindo: 

- Varredura de portas 🕵️‍♂️
- Análises de vulnerabilidades 🐛  
- Checagens de escalação de privilégios 🔐
- Revisões de configuração ⚙️

Com o Auditor de Segurança Docker, times de segurança podem facilmente auditar imagens Docker em busca de possíveis problemas.

## Como usar 🤔

1. Inspecionar imagem

    docker build -t auditor .
    docker run --rm -v /var/run/docker.sock:/var/run/docker.sock auditor scan minhaimagem
2. Testes automatizados

    docker-compose up --build -d TARGET_IP=xxx.xxx.x.x
Isso sim permitiria testes FULLY AUTOMATIZADOS, reprodutíveis e em larga escala!

Uma ótima solução para implementar sistemas de segurança automáticos e certificações. 

3. Veja os resultados 📊

Os resultados da varredura serão impressos no stdout. Você também pode ver um relatório no HTML.

## Contribuindo 👥

Pull requests são bem-vindos! Confira o [CONTRIBUTING.md](CONTRIBUTING.md) para as diretrizes.

## Suporte 🤝

Precisa de ajuda? Abra uma [issue](https://github.com/scoobiii/docker-audit/issues) no GitHub.  

# Estrutura

        |- docs/
        |- test/  
        |- src/
        |- modules/portscan.js
        |- modules/test.js
        |- Dockerfile
        |- docker-compose.yml
        |- README.md

## Créditos 🙏

Feito com ❤️ pelo [@scoobiii & kali-tool-explorer & Bard & Claude ](https://github.com/scoobiii)