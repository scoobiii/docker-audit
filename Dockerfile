FROM node:16-alpine

# Instalar ferramentas
RUN apk add --no-cache nmap bandit trufflehog tfsec

# Copiar código
COPY . .

# Build da imagem
RUN npm install

# Definir variáveis de ambiente
ENV NODE_ENV production

# Comando de entrada
CMD ["node", "app.js"]
