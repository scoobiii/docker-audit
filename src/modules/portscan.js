const nmap = require('nmap');

function portScan(image) {
  // Lógica de varredura de portas da imagem
  const scanner = new nmap.Scanner();
  scanner.scan(image, {
    // Opções de varredura
    ports: '1-65535',
    // Tempo limite de espera
    timeout: 1000,
  }, (err, result) => {
    if (err) {
      console.error('Erro ao escanear portas:', err);
      return;
    }

    const openPorts = [];
    for (const host in result) {
      for (const port in result[host].ports) {
        if (result[host].ports[port].state === 'open') {
          openPorts.push({
            host,
            port: parseInt(port),
          });
        }
      }
    }

    console.log('Portas abertas:');
    console.table(openPorts);
  });
}

module.exports = {
  portScan,
}; 
