const { portScan } = require('../modules/portscan')

test('detects open port 80', () => {
  const ports = portScan('nginx')  
  expect(ports).toEqual([80])
})