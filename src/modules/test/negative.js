const { portScan } = require('../portscan');

describe('Teste de varredura de portas - Casos Negativos', () => {
  it('Deve não encontrar portas abertas em uma imagem sem portas abertas', async () => {
    const image = 'alpine';
    const openPorts = await portScan(image);
    expect(openPorts.length).toBe(0);
  });
});
