const { portScan } = require('../portscan');

describe('Teste de varredura de portas - Casos Positivos', () => {
  it('Deve encontrar portas abertas em uma imagem com portas abertas', async () => {
    const image = 'busybox';
    const openPorts = await portScan(image);
    expect(openPorts.length).toBeGreaterThan(0);
  });
});
